# The MIT License (MIT)
#
# Copyright (c) 2017 Thorsten Simons (sw@snomis.de)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from os import urandom, mkdir
from os.path import join

SIZE = 10
TGT = 'folder'
LEVEL1 = 0
LEVEL2 = 0
LEVEL3 = 0
PERLEVEL = 100000

DATA = urandom(SIZE)

WRITTEN = 0

def writefiles(path):
    global WRITTEN

    for i in range(PERLEVEL):
        with open(join(path, str(i))+'.dat', 'wb') as outhdl:
            outhdl.write(DATA)
            WRITTEN += 1

if __name__ == '__main__':

    mkdir(TGT)
    writefiles(TGT)
    for l1 in range(LEVEL1):
        mkdir(join(TGT, str(l1)))
        writefiles(join(TGT, str(l1)))
        for l2 in range(LEVEL2):
            mkdir(join(TGT, str(l1), str(l2)))
            writefiles(join(TGT, str(l1), str(l2)))
            for l3 in range(LEVEL3):
                mkdir(join(TGT, str(l1), str(l2), str(l3)))
                writefiles(join(TGT, str(l1), str(l2), str(l3)))

    print('wrote {} files'.format(WRITTEN))


