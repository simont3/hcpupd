Limitations
===========

*   Moving a folder structure into the *watchdir*...

    ...will lead to the files in the top-level folder to be uploaded, but
    everything else will not. Reason for this is that the inotify mechanism
    in charge is not getting the information for all the sub-folders when
    moving in a folder structure.

    Workaround: avoid moving in folder structures, copy them instead.

*   Renaming a folder (or moving a folder within *watchdir*) is not supported.

