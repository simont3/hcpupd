Configuration
=============

**hcpupd**'s behavior is controlled by a configuration file, which is searched
for in this order:

If specified, the file identified by the ``-c`` parameter, otherwise

    1.  Current working directory: ``.hcpupd.conf``
    2.  User's home directory: ``~/.hcpupd.conf``
    3.  System-wide: ``/var/lib/misc/hcpupd.conf``


..  Tip::

    **hcpupd** will offer to create a template configuration file in case it
    can't find one in any of the given locations::

        $ hcpupd
        No configuration file found.
        Do you want me to create a template file in the current directory (y/n)? y

        A template file (.hcpup.conf) has been created in the current directory.
        Please edit it to fit your needs...


The configuration file explained
--------------------------------

The configuration file is an ini-style text file with several sections, each
of them holding configuration values.

The [src] section
^^^^^^^^^^^^^^^^^

describes from where files are to be uploaded to HCP, and how.

::

    [src]
    watchdir = /watchdir
    upload existing files = yes
    delete after upload = yes
    remove empty folders after upload = yes


*   **watchdir** - is the folder that will be monitored; every file written
    into it will be uploaded to HCP.
    **The folder specified here must exist when hcpupd is started.**
*   **upload existing files** - enable discovery of files that are already in
    ``watchdir`` when **hcpupd** is started.
*   **delete after upload** - enabled auto-deletion of files as soon as they
    have been uploaded successfully.
*   **remove empty folders after upload** - enable auto-deletion of folders
    as soon as the last file has been uploaded.

..  Warning::

    Be aware that setting ``remove empty folders after upload = yes`` will
    cause **hcpupd** to immediately delete a folder when the last file it
    contained has been uploaded.

    **This may cause applications writing into the watchdir to fail, as they
    might still expect a folder to exist they created earlier.**


The [tgt] section
^^^^^^^^^^^^^^^^^

describes where to store the files found in [src], and how.

::

    [tgt]
    namespace = namespace.tenant.hcp.domain.com
    path = hcpupd/application
    user = username
    password = her_password
    ssl = yes
    obfuscate = yes
    local DNS resolver = no
    upload threads = 2

*   **namespace** - the HCP Namespace to write to
*   **path** - the path within the Namespace
*   **user** - a user with write access to the Namespace
*   **password** - her password
*   **ssl** - enable transfer encryption (HTTPS)
*   **obfuscate** - enable obfuscation of the file names stored to HCP
*   **local DNS resolver** - set to ``no`` to use the built-in resolver
*   **upload threads** - the number of uploader threads to use


The [meta] section
^^^^^^^^^^^^^^^^^^

describes how to build the custom metadata annotation stored with each files
(if ``obfuscate = yes``, only).

::

    [meta]
    annotation = hcpupd
    tag_timestamp = yes
    tag_note = files from my application
    retention = 0

*   **annotation** - the name of the annotation to write
*   **tag_timestamp** - enable adding the file's creation time
*   **tag_note** - a note that will be added
*   **retention** - ``0`` (zero) - the only supported value at this time


The [log] section
^^^^^^^^^^^^^^^^^

defines the logfile to write and if extended debug logging
shall be performed.

::

    [log]
    logfile = /var/log/hcpupd.log
    log uploaded files = yes
    debug = yes

*   **log uploaded files** - this will enable logging of uploaded files even if
    *debug = no*

..  Tip::

    Make sure to create the folder into which the ``logfile`` shall be stored
    before you start **hcpupd** the first time!


