Installation
============

Binary Distribution
-------------------

For most modern Linux derivates, you should be able to simply run the binary
provided
`here <https://gitlab.com/simont3/hcpupd/blob/master/src/dist/hcpupd>`_.

Grab it `there <https://gitlab.com/simont3/hcpupd/blob/master/src/dist/hcpupd>`_
and follow the instructions in chapter :doc:`30_run`.


Build your own Binary
---------------------

In case the provided binary fails to run on your Linux, you need to build it on
your own. Here's how to do that:

..  Warning::

    Make sure the ``objcopy`` utility is installed::

        $ objcopy

    If the command isn't found, you need to install the *GNU binutils* package.
    For Fedora 24, this is::

        $ sudo dnf install binutils.x86_64

*   Clone the repository from
    `GitLab <https://gitlab.com/simont3/hcpupd.git>`_::

        $ git clone https://gitlab.com/simont3/hcpupd.git

*   Change into the project folder and create a Python 3 virtual environment
    and activate it::

        $ cd hcpupd/src

*   Update pip and setuptools, then load all required dev-packages::

        $ sudo pip3 install --upgrade pip setuptools
        $ sudo pip3 install -r pip-requirements-dev.txt

*   Run the build tool::

        $ pyinstaller hcpupd.spec

    You should find the executable in the ``dist`` subfolder.

*   Now follow the instructions in chapter :doc:`30_run`.
