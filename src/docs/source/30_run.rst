Run **hcpupd**
==============

In the foreground
-----------------

Easiest way to run **hcpupd** is having a private ``~/.hcpupd.conf``
file and simply starting it::

    $ ./hcpupd

As a Daemon
-----------

Adding ``-d`` to the command will run **hcpupd** in daemon mode, releasing the
terminal session for further use::

    $ ./hcpupd -d

..  Warning::

    Due to the foundation technology, this will fail silently (!) if started by
    user **root**!

Using **systemd**
-----------------

In a unattended production environment, you'll want to run **hcpupd** as a
daemon in the background, automatically started on system boot.

..  Warning::

    For security reasons, you shouldn't run **hcpupd** with root privileges.
    You might consider to create a technical user for it...

    We assume that you have *<user>* and its *<group>* created, already...

*   Move the **hcpupd** binary to ``/usr/local/bin`` and set proper
    permissions::

        $ sudo cp hcpupd /usr/local/bin
        $ sudo chmod 755 /usr/local/bin/hcpupd

*   Create a *systemd* service file (``/etc/systemd/system/hcpupd.service``)
    with this content::

        [Unit]
        Description=HCP upload daemon
        # tested with Fedora 24:
        Requires=network.service
        # tested with Ubuntu 17.04:
        # Requires=networking.service

        [Service]
        Type=simple
        # make sure you set the correct path for bash!
        ExecStart=/usr/bin/bash -c /usr/local/bin/hcpupd
        Restart=always

        User=<user>
        Group=<group>

        [Install]
        WantedBy=multi-user.target

    **hcpupd** requires networking up and running, so you might need to find
    the proper entry for *[Unit] Requires=*, depending on your Linux
    distribution.

*   Run **hcpupd** once to create a template config file::

        $ hcpupd
        No configuration file found.
        Do you want me to create a template file in the current directory (y/n)? y

        A template file (.hcpup.conf) has been created in the current directory.
        Please edit it to fit your needs...

*   Edit the ``.hcpupd.conf`` template file to your specific needs.

    ..  Warning::

        Make sure that you have created the folder to watch, as well as the
        folder to store the logfile with appropriate permissions
        (see Config section).

*   Move the config file to it's place and set the permissions properly
    (**remember, there's a password in it - you don't want to expose it**)::

        $ sudo mv .hcpupd.conf /var/lib/misc/hcpupd.conf
        $ sudo chown <user>:<group> /var/lib/misc/hcpupd.conf
        $ sudo chmod 600 /var/lib/misc/hcpupd.conf

*   If you stay with the default logging path set in the config file, create
    the folder::

        $ sudo mkdir /var/log/hcpupd
        $ sudo chown <user>:<group> /var/log/hcpupd
        $ sudo chmod 700 /var/log/hcpupd

*   Manually start **hcpupd** in foreground mode to give it a try::

        $ sudo -u <user> /usr/local/bin/hcpupd

    If everything works fine (which means the config file is correct), stop
    **hcpupd** by pressing CTRL-C.

*   Now start it as a daemon using *systemd*::

        $ sudo systemctl start hcpupd.service

    Check if it's running by::

        $ sudo systemctl status hcpupd.service

*   If everything is fine, you can enable the daemon to be started on Linux
    boot::

        $ sudo systemctl enable hcpupd.service
