Release History
===============

**0.2.5 2017-06-11**

*   fixed a bug that caused installation through pip to fail
*   changed documentation telling not to run **hcpupd** as root, plus some
    systemd-related info

**0.2.4 2017-03-27**

*   in case of inotify queue overflow, a directory scan is triggered to
    make sure no new files get lost
*   in case the queue runs empty, we now preventively trigger a directory scan,
    as well
*   new config item 'log uploaded files' allows to log uploaded files in
    non-debug logging mode
*   added a message on shutdown that tells how many files are left for later
    upload

**0.2.3 2017-03-01**

*   re-factored configuration file handling
*   now surviving connection loss to HCP (missed file recovery still requires
    *hcpupd* restart)

**0.2.2 2017-02-15**

*   fixed a bug that caused moved_in folders not to be processed
*   added some more debug output for watched folder handling

**0.2.1 2017-02-12**

*   various fixes related to publishing throuh gitlab and readthedocs.org

**0.2.0 2017-02-11**

*   First public release

